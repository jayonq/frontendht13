const url = "https://random-data-api.com/api/v2/users?size=10";

const createUserMarkup = (user, container) => {
  const userDiv = document.createElement("div");
  const userPhoto = document.createElement("img");
  const userInfoDiv = document.createElement("div");
  const userCover = document.createElement("div");
  const userName = document.createElement("h2");
  const userEmail = document.createElement("p");
  const userText = document.createElement("p");

  userDiv.classList.add("user");
  userPhoto.classList.add("user-avatar");
  userInfoDiv.classList.add("user-info");
  userCover.classList.add("user-cover");
  userName.classList.add("user-name");
  userEmail.classList.add("user-email");

  userText.classList.add("user-text");

  userDiv.appendChild(userPhoto);
  userDiv.appendChild(userInfoDiv);
  userDiv.appendChild(userCover);
  userCover.appendChild(userPhoto);
  userInfoDiv.appendChild(userName);
  userInfoDiv.appendChild(userEmail);

  userInfoDiv.appendChild(userText);

  userPhoto.src = user.avatar;
  userPhoto.alt = `${user.first_name} ${user.last_name}`;

  userName.textContent = `${user.first_name} ${user.last_name}`;
  userEmail.textContent = user.email;
  userText.textContent =
    "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Necessitatibus fugit reiciendis beatae eligendi officia nulla porro repellendus, explicabo, laborum laudantium, rem quae. Aut totam laudantium ratione incidunt hic, provident perspiciatis. Quasi voluptatem doloribus laborum necessitatibus quisquam enim quam.";

  userDiv.appendChild(userInfoDiv);

  container.appendChild(userDiv);
};

const loadUsers = async () => {
  try {
    const users = await axios.get(url);
    const usersContainer = document.getElementById("user__list");
    users.data.map((user) => {
      createUserMarkup(user, usersContainer);
    });
  } catch (e) {
    console.error(e);
  }
};

loadUsers();
